# SST-Lite 1 firmware
This repo contains the STM32CubeIDE project for SST-1 Lite V1_0.

## Serial commands

- **Start:**
	- `A\n`
- **Stop:**
	- `B\n`
- **Set direction:**
	- From right to left:
		- `C\n`
	- From left to right:
		- `D\n`
- **Set mode:**
	- Single mode:
		- `E\n`
	- Column mode:
		- `F\n`
- **Set frame duration in microseconds:**
	- `Gx\n`
		- x = frame duration in microseconds
			- range: 60...99999
- **Set brightness:**
	- `Hx\n`
		- x = brightness value
			- range: 10%...100%
- **Set input trigger edge:**
	- `Ix\n`
		- x = 0: ![Falling edge GUI icon](assets/falling.png) falling edge (default)
		- x = 1: ![Rising edge GUI icon](assets/rising.png) rising edge
- **Set output trigger edge:**
	- `Jx\n`
		- x = 0: normal output, falling edge on first frame (default, no icon displayed)
		- x = 1: ![Inverted output icon](assets/inverted.png) inverted output, rising edge on first frame 

## Output trigger
- Latency: 4.1 µs
- Trigger pulse length: 30 µs

## Input trigger
- Latency: 64.4

---

## Hardware
PC8:
- OLED address selection pin. 
- Sets the OLED address used on startup.
- Uses an internal pull-up.
- if HIGH, ssd1306_Init(0x78);
- if LOW, ssd1306_Init(0x7A);

---
