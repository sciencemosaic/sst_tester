/*
 * serial_protocol.h
 *
 *  Created on: Mar 24, 2022
 *      Author: Kasutaja
 */

#ifndef INC_SERIAL_PROTOCOL_H_
#define INC_SERIAL_PROTOCOL_H_

#include "../Inc/LEDMatrix.h"
#include "stm32f4xx_hal.h"
#include "main.h"
#include "string.h"

#define CMD_START           		65   // 'A'
#define CMD_STOP            		66   // 'B'
#define CMD_SET_DIR_RIGHT   		67   // 'C'
#define CMD_SET_DIR_LEFT    		68   // 'D'
#define CMD_SET_MODE_SINGLE    		69   // 'E'
#define CMD_SET_MODE_COLUMN    		70   // 'F'
#define CMD_SET_FRAME_LENGTH		71   // 'G'
#define CMD_SET_BRIGHTNESS			72   // 'H'
#define CMD_SET_INPUT_TRIGGER_EDGE	73   // 'I'
#define CMD_SET_OUTPUT_TRIGGER_EDGE	74   // 'J'
#define NEWLINE						10   // '\n'


void parse_serial(uint8_t* Buf);

#endif /* INC_SERIAL_PROTOCOL_H_ */
