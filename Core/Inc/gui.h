/*
 * gui.h
 *
 *  Created on: 23 Feb 2022
 *      Author: Kasutaja
 */

#ifndef INC_GUI_H_
#define INC_GUI_H_

#include "main.h"
#include "LEDMatrix.h"

#define ARROW_X		116
#define ARROW_Y		56

#define MODE_X		2
#define MODE_Y		54

#define INPUT_EDGE_X		96
#define INPUT_EDGE_Y		50

#define OUTPUT_EDGE_X		78
#define OUTPUT_EDGE_Y		50

void draw_direction(void);
void draw_mode(void);
void draw_us(uint32_t _step_us);
void draw_freq(uint32_t _step_us);
void draw_duty(void);
void draw_input_trigger_edge(void);
void draw_output_trigger_edge(void);
void draw_main_view(void);
void draw_duty_view(void);
void update_gui(void);

#endif /* INC_GUI_H_ */
