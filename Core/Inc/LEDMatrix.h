/*
 * LEDMatrix.h
 *
 *  Created on: Feb 18, 2022
 *      Author: Kasutaja
 */

#ifndef SRC_LEDMATRIX_H_
#define SRC_LEDMATRIX_H_

#include "stm32f4xx_hal.h"
#include "main.h"

#define LED_COUNT 			20
#define LED_ROW_COUNT		6

#define LEDS_5V_SUPPLY		1
#define LEDS_DISABLE_SUPPLY 2

#define LEDS_STOPPED		0
#define LEDS_RUNNING		1

#define LED_DIR_LEFT		0
#define LED_DIR_RIGHT		1

#define SR_DATA_LEFT		(uint32_t)0b10000000000000000000
#define SR_DATA_RIGHT		(uint32_t)0b00000000000000000001

#define DISABLE_SUPPLY		0
#define ENABLE_SUPPLY		1

#define SR_DISABLED			0
#define SR_ENABLED			1

#define LED_ROWS_DISABLED   0
#define LED_ROW_1			1 << 0
#define LED_ROW_2			1 << 1
#define LED_ROW_3			1 << 2
#define LED_ROW_4			1 << 3
#define LED_ROW_5			1 << 4
#define LED_ROW_6			1 << 5
#define LED_ALL_ROWS		LED_ROW_1 | LED_ROW_2 | LED_ROW_3 | LED_ROW_4 | LED_ROW_5 | LED_ROW_6

#define LED_MODE_SINGLE					0
#define LED_MODE_COLUMN					1

#define STEP_MAX_US						99999
#define STEP_MIN_US						60
#define DEFAULT_STEP_US					100

#define STEP_SLOW_SPEED_THRESHOLD		10000

#define ENCODER_FINE_STEP				1
#define ENCODER_COARSE_STEP				25

#define PWM_DUTY_LEVELS 				10
#define PWM_DUTY_STEP					100 / 10
#define PWM_DUTY_ACTIVE_THRESHOLD		300

#define TRIGGER_EDGE_FALLING			0
#define TRIGGER_EDGE_RISING				1

extern uint32_t step_us;
extern uint32_t step_us_buffer;

extern uint32_t led_direction;
extern uint32_t led_direction_buffer;

extern uint32_t led_mode;
extern uint32_t led_mode_buffer;

extern uint32_t duty;
extern uint32_t duty_buffer;

extern uint32_t duty_us_len;

extern uint32_t input_trigger_edge;
extern uint32_t input_trigger_edge_buffer;

extern uint32_t output_trigger_edge;
extern uint32_t output_trigger_edge_buffer;

extern uint32_t leds_running;

void Leds_Init(uint32_t supply, uint32_t mode, uint32_t direction);
void Leds_Set_Supply(uint32_t supply);
void Leds_Set_Rows(uint8_t rows);
//void Leds_Set_First_Row(void);
void Leds_Set_All_Rows();
void Leds_Init_Direction(uint32_t direction);
void Leds_Set_Mode(uint32_t mode);
void Leds_Toggle_Mode(void);
void Leds_Toggle_Direction(void);
void Leds_Rotate(void);
void Leds_Set_Duty(uint32_t _interval);
void Leds_Set_Input_Trigger_Edge(uint32_t _edge);
void Leds_Set_Output_Trigger_Edge(uint32_t _edge);
void Leds_Reset(void);
uint8_t Get_Leds_Running(void);
void Set_Leds_Running(uint8_t state);
void Trigger_Output(void);
//void Leds_Start(TIM_HandleTypeDef * htim);
//void Leds_Stop(TIM_HandleTypeDef * htim);


#endif /* SRC_LEDMATRIX_H_ */
