/* USER CODE BEGIN Header */
/**
  ******************************************************************************
  * @file           : main.h
  * @brief          : Header for main.c file.
  *                   This file contains the common defines of the application.
  ******************************************************************************
  * @attention
  *
  * Copyright (c) 2022 STMicroelectronics.
  * All rights reserved.
  *
  * This software is licensed under terms that can be found in the LICENSE file
  * in the root directory of this software component.
  * If no LICENSE file comes with this software, it is provided AS-IS.
  *
  ******************************************************************************
  */
/* USER CODE END Header */

/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __MAIN_H
#define __MAIN_H

#ifdef __cplusplus
extern "C" {
#endif

/* Includes ------------------------------------------------------------------*/
#include "stm32f4xx_hal.h"

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */
#include <LEDMatrix.h>
#include "ssd1306.h"
#include "ssd1306_fonts.h"
#include "ssd1306_tests.h"
#include "icons.h"
#include "serial_protocol.h"
/* USER CODE END Includes */

/* Exported types ------------------------------------------------------------*/
/* USER CODE BEGIN ET */

/* USER CODE END ET */

/* Exported constants --------------------------------------------------------*/
/* USER CODE BEGIN EC */

/* USER CODE END EC */

/* Exported macro ------------------------------------------------------------*/
/* USER CODE BEGIN EM */

/* USER CODE END EM */

void HAL_TIM_MspPostInit(TIM_HandleTypeDef *htim);

/* Exported functions prototypes ---------------------------------------------*/
void Error_Handler(void);

/* USER CODE BEGIN EFP */
void Update_Timer_Interval(uint32_t interval_us);
void Leds_Start(void);
void Leds_Stop(void);

void Led_Start_Power(void);
void Led_Stop_Power(void);
void Set_Compare_Value(uint32_t compare_value);
void Start_GUI_Duty_View_Timer(void);
/* USER CODE END EFP */

/* Private defines -----------------------------------------------------------*/
#define en_row_1_Pin GPIO_PIN_0
#define en_row_1_GPIO_Port GPIOA
#define en_row_2_Pin GPIO_PIN_1
#define en_row_2_GPIO_Port GPIOA
#define en_row_3_Pin GPIO_PIN_2
#define en_row_3_GPIO_Port GPIOA
#define en_row_4_Pin GPIO_PIN_3
#define en_row_4_GPIO_Port GPIOA
#define en_row_5_Pin GPIO_PIN_4
#define en_row_5_GPIO_Port GPIOA
#define en_row_6_Pin GPIO_PIN_5
#define en_row_6_GPIO_Port GPIOA
#define sr_data_Pin GPIO_PIN_6
#define sr_data_GPIO_Port GPIOA
#define sr_latch_Pin GPIO_PIN_7
#define sr_latch_GPIO_Port GPIOA
#define sr_clock_Pin GPIO_PIN_4
#define sr_clock_GPIO_Port GPIOC
#define sr_enable_Pin GPIO_PIN_5
#define sr_enable_GPIO_Port GPIOC
#define sw_enc_Pin GPIO_PIN_0
#define sw_enc_GPIO_Port GPIOB
#define sw_enc_EXTI_IRQn EXTI0_IRQn
#define enc_b_Pin GPIO_PIN_1
#define enc_b_GPIO_Port GPIOB
#define enc_b_EXTI_IRQn EXTI1_IRQn
#define enc_c_Pin GPIO_PIN_10
#define enc_c_GPIO_Port GPIOB
#define en_5v_Pin GPIO_PIN_11
#define en_5v_GPIO_Port GPIOB
#define debug_led_Pin GPIO_PIN_12
#define debug_led_GPIO_Port GPIOB
#define enc_a_Pin GPIO_PIN_15
#define enc_a_GPIO_Port GPIOB
#define enc_a_EXTI_IRQn EXTI15_10_IRQn
#define dbg_out_Pin GPIO_PIN_6
#define dbg_out_GPIO_Port GPIOC
#define dbg_out2_Pin GPIO_PIN_7
#define dbg_out2_GPIO_Port GPIOC
#define oled_addr_sel_Pin GPIO_PIN_8
#define oled_addr_sel_GPIO_Port GPIOC
#define ttl_out_Pin GPIO_PIN_10
#define ttl_out_GPIO_Port GPIOC
#define ttl_in_Pin GPIO_PIN_12
#define ttl_in_GPIO_Port GPIOC
#define ttl_in_EXTI_IRQn EXTI15_10_IRQn
#define sw_r_Pin GPIO_PIN_5
#define sw_r_GPIO_Port GPIOB
#define sw_r_EXTI_IRQn EXTI9_5_IRQn
#define sw_l_Pin GPIO_PIN_8
#define sw_l_GPIO_Port GPIOB
#define sw_l_EXTI_IRQn EXTI9_5_IRQn

/* USER CODE BEGIN Private defines */

extern uint8_t duty_edit_mode_enabled;
extern uint8_t duty_edit_mode_enabled_buffer;

/* USER CODE END Private defines */

#ifdef __cplusplus
}
#endif

#endif /* __MAIN_H */
