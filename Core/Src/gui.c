/*
 * gui.c
 *
 *  Created on: 23 Feb 2022
 *      Author: Kasutaja
 */

#include "gui.h"

void draw_direction(void) {
	if (led_direction_buffer == LED_DIR_RIGHT) {
		ssd1306_DrawBitmap(ARROW_X, ARROW_Y, left_arrow, ICON_W, ICON_H, Black);
		ssd1306_DrawBitmap(ARROW_X, ARROW_Y, right_arrow, ICON_W, ICON_H, White);
	} else if (led_direction_buffer == LED_DIR_LEFT) {
		ssd1306_DrawBitmap(ARROW_X, ARROW_Y, right_arrow, ICON_W, ICON_H, Black);
		ssd1306_DrawBitmap(ARROW_X, ARROW_Y, left_arrow, ICON_W, ICON_H, White);
	}
}

void draw_mode(void) {
	if (led_mode_buffer == LED_MODE_SINGLE) {
		ssd1306_SetCursor(MODE_X, MODE_Y);
		ssd1306_WriteString("SINGLE  ", Font_7x10, White);
	} else if (led_mode_buffer == LED_MODE_COLUMN) {
		ssd1306_SetCursor(MODE_X, MODE_Y);
		ssd1306_WriteString("COLUMN  ", Font_7x10, White);
	}
}

void draw_us(uint32_t _step_us) {
	char step_us_str[16];
	sprintf(step_us_str, "%ius      ", _step_us);

	ssd1306_SetCursor(2, 8);
	ssd1306_WriteString(step_us_str, Font_16x26, White);
}

void draw_freq(uint32_t _step_us) {
	float freq = 1.0 / ((float)_step_us * 0.000001) ;
	char freq_str[16];
	sprintf(freq_str, "%.2fHz   ", freq);
	ssd1306_SetCursor(2, 32);
	ssd1306_WriteString(freq_str, Font_11x18, White);
}

void draw_duty(void) {
	ssd1306_SetCursor(2, 8);
	ssd1306_WriteString("BRIGHTNESS", Font_7x10, White);

	char brightness_str[16];

	ssd1306_SetCursor(2, 28);
	sprintf(brightness_str, "%i%%  ", duty_buffer); // double %% = '%'

	ssd1306_WriteString(brightness_str, Font_16x26, White);
}

void draw_input_trigger_edge(void) {
	if (input_trigger_edge_buffer == TRIGGER_EDGE_FALLING) {
		ssd1306_DrawBitmap(INPUT_EDGE_X, INPUT_EDGE_Y, rising_edge_icon, ICON_W, ICON_H, Black);
		ssd1306_DrawBitmap(INPUT_EDGE_X, INPUT_EDGE_Y, falling_edge_icon, ICON_W, ICON_H, White);
	} else if (input_trigger_edge_buffer == TRIGGER_EDGE_RISING) {
		ssd1306_DrawBitmap(INPUT_EDGE_X, INPUT_EDGE_Y, falling_edge_icon, ICON_W, ICON_H, Black);
		ssd1306_DrawBitmap(INPUT_EDGE_X, INPUT_EDGE_Y, rising_edge_icon, ICON_W, ICON_H, White);
	}
}


void draw_output_trigger_edge(void) {
	if (output_trigger_edge_buffer == TRIGGER_EDGE_FALLING) {
		ssd1306_DrawBitmap(OUTPUT_EDGE_X, OUTPUT_EDGE_Y, not_icon, ICON_W, ICON_H, Black);
	} else if (output_trigger_edge_buffer == TRIGGER_EDGE_RISING) {
		ssd1306_DrawBitmap(OUTPUT_EDGE_X, OUTPUT_EDGE_Y, not_icon, ICON_W, ICON_H, White);
	}
}

void draw_main_view(void) {
	ssd1306_Fill(Black);
	draw_us(step_us_buffer);
	draw_freq(step_us_buffer);
	draw_direction();
	draw_input_trigger_edge();
	draw_output_trigger_edge();
	draw_mode();
	ssd1306_UpdateScreen();
}

void draw_duty_view(void) {
	ssd1306_Fill(Black);
	draw_duty();
	ssd1306_UpdateScreen();
}

void update_gui(void) {
	if (duty_edit_mode_enabled != duty_edit_mode_enabled_buffer) { // view changed -> redraw
		duty_edit_mode_enabled_buffer = duty_edit_mode_enabled;


		if (duty_edit_mode_enabled_buffer == 0) {
			// redraw main view
			draw_main_view();
		} else {
			// redraw duty view
			draw_duty_view();
		}
	} else if (step_us != step_us_buffer) {
		step_us_buffer = step_us;

		// update led matrix frame timer interval
		Update_Timer_Interval(step_us_buffer);

		// redraw frame length and freq
		draw_us(step_us_buffer);
		draw_freq(step_us_buffer);
		ssd1306_UpdateScreen();
	} else if (led_direction != led_direction_buffer) {
		led_direction_buffer =  led_direction;
		draw_direction();
		ssd1306_UpdateScreen();
	} else if (led_mode != led_mode_buffer) {
		led_mode_buffer = led_mode;
		draw_mode();
		ssd1306_UpdateScreen();
	} else if (input_trigger_edge != input_trigger_edge_buffer) {
		input_trigger_edge_buffer = input_trigger_edge;
		draw_input_trigger_edge();
		ssd1306_UpdateScreen();
	} else if (output_trigger_edge != output_trigger_edge_buffer) {
		output_trigger_edge_buffer = output_trigger_edge;
		draw_output_trigger_edge();
		ssd1306_UpdateScreen();
	} else if (duty != duty_buffer) {
		duty_buffer = duty;
		Leds_Set_Duty(step_us_buffer);
		draw_duty_view();
	}
}

