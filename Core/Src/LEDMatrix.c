/*
 * LEDMatrix.cpp
 *
 *  Created on: Feb 18, 2022
 *      Author: Kasutaja
 */

#include "../Inc/LEDMatrix.h"

extern inline void SR_Clock_Pulse(void);
extern inline void SR_Shift_Data_In(uint32_t data, uint32_t len);
extern inline void SR_Shift_Bit_In(uint32_t bit);
extern inline void SR_Latch(void);
extern inline void SR_Clear(void);
extern inline void SR_Enable(void);
extern inline void SR_Disable(void);
extern inline void Leds_Update_Row(void);
//extern inline void SR_Set_Data(uint32_t data);
//extern inline void SR_Rotate_Left(void);

extern TIM_HandleTypeDef htim14;

//uint32_t porta_states = 0;
uint32_t sr_data = 0;
uint32_t sr_data_offset = 0;
uint32_t led_rows = LED_ALL_ROWS;
uint32_t led_direction = LED_DIR_LEFT;
uint32_t led_direction_buffer = LED_DIR_LEFT;
uint32_t leds_running = LEDS_STOPPED;
uint32_t led_mode = LED_MODE_COLUMN;
uint32_t led_mode_buffer = LED_MODE_COLUMN;

uint32_t step_us = DEFAULT_STEP_US;
uint32_t step_us_buffer = 0;

uint32_t duty = 100;
uint32_t duty_buffer = 100;
uint32_t duty_us_len = 0;

uint32_t input_trigger_edge = TRIGGER_EDGE_FALLING;
uint32_t input_trigger_edge_buffer = TRIGGER_EDGE_FALLING;

uint32_t output_trigger_edge = TRIGGER_EDGE_FALLING;
uint32_t output_trigger_edge_buffer = TRIGGER_EDGE_FALLING;

// initialize leds
void Leds_Init(uint32_t supply, uint32_t mode, uint32_t direction) {
	SR_Enable();
	SR_Clear();
	Leds_Set_Supply(supply);
	Leds_Set_Mode(mode);
	Leds_Set_Output_Trigger_Edge(output_trigger_edge);
	Leds_Init_Direction(direction);
}

// sets supply to 3V3, 5V or disables it
void Leds_Set_Supply(uint32_t supply) {
	if (supply == LEDS_5V_SUPPLY) {
//		HAL_GPIO_WritePin(en_5v_GPIO_Port, en_5v_Pin, ENABLE_SUPPLY);
		Led_Start_Power();
	} else if (supply == LEDS_DISABLE_SUPPLY) {
		Led_Stop_Power();
//		HAL_TIM_PWM_Stop(&htim2, TIM_CHANNEL_4);
		HAL_GPIO_WritePin(en_5v_GPIO_Port, en_5v_Pin, DISABLE_SUPPLY);
	}
}


void Leds_Set_Duty(uint32_t _interval) {
	if (step_us_buffer < PWM_DUTY_ACTIVE_THRESHOLD) {
		Set_Compare_Value(step_us_buffer);
	} else {
		duty_us_len = step_us_buffer * ((float)duty_buffer / 100.0);
		Set_Compare_Value(duty_us_len);
	}
}

// Turns specified rows on and off
// e.g. use LED_ROW_1 | LED_ROW_4 to turn on rows 1 and 4
// Use LED_ALL_ROWS to turn on all rows.
// Use LED_ROWS_DISABLED to turn off all rows.
void Leds_Set_Rows(uint8_t rows) {
	GPIOA->ODR = (GPIOA->ODR & 0b11111111111111111111111111000000) | rows;
}

// Turns on all rows
void Leds_Set_All_Rows(void) {
	Leds_Set_Rows(LED_ALL_ROWS);
}

// Enable shift register
inline void SR_Enable(void) {
	HAL_GPIO_WritePin(sr_enable_GPIO_Port, sr_enable_Pin, SR_ENABLED);
}

// Disable shift register
inline void SR_Disable(void) {
	HAL_GPIO_WritePin(sr_enable_GPIO_Port, sr_enable_Pin, SR_DISABLED);
}

// Send clock pulse to shift data to SR
inline void SR_Clock_Pulse(void) {
	HAL_GPIO_WritePin(sr_clock_GPIO_Port, sr_clock_Pin, 1);
	HAL_GPIO_WritePin(sr_clock_GPIO_Port, sr_clock_Pin, 0);
}

// Shift data in. len specifies the data length, leftover most significant bits are discarded.
inline void SR_Shift_Data_In(uint32_t data, uint32_t len) {
	for (int i = 0; i < len; i++) {
		SR_Shift_Bit_In((data >> i) & 0x0001);
	}
}

// Shift a single bit in
inline void SR_Shift_Bit_In(uint32_t bit) {
	HAL_GPIO_WritePin(sr_data_GPIO_Port, sr_data_Pin, bit);
	SR_Clock_Pulse();
}

// Load the data in shift register buffer to its outputs.
inline void SR_Latch(void) {
	HAL_GPIO_WritePin(sr_latch_GPIO_Port, sr_latch_Pin, 1);
	HAL_GPIO_WritePin(sr_latch_GPIO_Port, sr_latch_Pin, 0);
}

// Clear shift registers
inline void SR_Clear(void) {
	  SR_Shift_Data_In(0x0000, LED_COUNT);
	  SR_Latch();
}

// Set and initialize LED direction.
// LED_DIR_LEFT of LED_DIR_RIGHT
void Leds_Init_Direction(uint32_t direction) {
	if (leds_running == LEDS_STOPPED) {
		if (direction == LED_DIR_LEFT) {
			led_direction = direction;
			sr_data = SR_DATA_LEFT;
			sr_data_offset = 0;
		} else if (direction == LED_DIR_RIGHT) {
			led_direction = direction;
			sr_data = SR_DATA_RIGHT;
			sr_data_offset = 0;
		}
	}
}

// Set and initialize LED mode
// LED_MODE_COLUMN or LED_MODE_SINGLE
void Leds_Set_Mode(uint32_t mode) {
	led_mode = mode;
	if (led_mode == LED_MODE_COLUMN) {
//		Leds_Set_All_Rows();
		led_rows = LED_ALL_ROWS;
	} else if (led_mode == LED_MODE_SINGLE) {
		led_rows = LED_ROW_1;
	}
}

// Toggle between LED_MODE_COLUMN and LED_MODE_SINGLE
void Leds_Toggle_Mode(void){
	if (led_mode == LED_MODE_COLUMN) {
		led_mode = LED_MODE_SINGLE;
	} else if (led_mode == LED_MODE_SINGLE) {
		led_mode = LED_MODE_COLUMN;
	}
	Leds_Set_Mode(led_mode);
}

// Toggle between right and left direction
void Leds_Toggle_Direction(void) {
	if (led_direction == LED_DIR_LEFT) {
		led_direction = LED_DIR_RIGHT;
		sr_data = SR_DATA_RIGHT;
	} else if (led_direction == LED_DIR_RIGHT) {
		led_direction = LED_DIR_LEFT;
		sr_data = SR_DATA_LEFT;
	}

	sr_data_offset = 0;
}

// Rotate leds
// Takes about 40us to execute
void Leds_Rotate(void) {
	SR_Shift_Data_In(sr_data, LED_COUNT);
	SR_Latch();
	Leds_Set_Rows(led_rows);

	if (sr_data_offset == 0) {
		if (((led_rows == LED_ROW_1) && (led_mode == LED_MODE_SINGLE)) || (led_mode == LED_MODE_COLUMN)) {
			Trigger_Output();
		}
	}

	if (led_direction == LED_DIR_LEFT) {
		if (sr_data_offset == (LED_COUNT - 1)) {
			sr_data = SR_DATA_LEFT;
			sr_data_offset = 0;
			Leds_Update_Row();
		} else {
			sr_data = sr_data >> 1;
			sr_data_offset++;
		}
	} else if (led_direction == LED_DIR_RIGHT) {
		if (sr_data_offset == (LED_COUNT - 1)) {
			sr_data = SR_DATA_RIGHT;
			sr_data_offset = 0;
			Leds_Update_Row();
		} else {
			sr_data = sr_data << 1;
			sr_data_offset++;
		}
	}
}

// Update row
// Used to update the active row in single led mode at the end of a full 20 step cycle.
inline void Leds_Update_Row(void) {
	if (led_mode == LED_MODE_SINGLE) {
		if (led_rows == LED_ROW_6) {
			led_rows = LED_ROW_1;
		} else {
			led_rows = led_rows << 1;
		}
	}
}

uint8_t Get_Leds_Running(void) {
	return leds_running;
}

void Set_Leds_Running(uint8_t state) {
	leds_running = state;
}


void Leds_Set_Input_Trigger_Edge(uint32_t _edge) {
	input_trigger_edge = _edge;
}

void Leds_Set_Output_Trigger_Edge(uint32_t _edge) {
	output_trigger_edge = _edge;
	HAL_GPIO_WritePin(ttl_out_GPIO_Port, ttl_out_Pin, output_trigger_edge);
}

void Leds_Reset(void) {
	Leds_Init_Direction(led_direction);
	if (led_mode == LED_MODE_SINGLE) {
			led_rows = LED_ROW_1;
	}
}

void Trigger_Output(void) {
	__HAL_TIM_CLEAR_FLAG(&htim14, TIM_FLAG_UPDATE); // don't interrupt immediately, wait timer to interrupt
	HAL_GPIO_WritePin(ttl_out_GPIO_Port, ttl_out_Pin, (output_trigger_edge != TRIGGER_EDGE_RISING));
	HAL_TIM_Base_Start_IT(&htim14); // start output trigger pulse timer, stops itself after first interrupt
}

//// Start the led timer
//void Leds_Start(TIM_HandleTypeDef * htim) {
//	if (leds_running == LEDS_STOPPED) {
//		leds_running = LEDS_RUNNING;
//		HAL_TIM_Base_Start_IT(htim);
//	}
//}
//
//// Stop the led timer
//void Leds_Stop(TIM_HandleTypeDef * htim) {
//	if (leds_running == LEDS_RUNNING) {
//		leds_running = LEDS_STOPPED;
//		HAL_TIM_Base_Stop_IT(htim);
//	}
//}
