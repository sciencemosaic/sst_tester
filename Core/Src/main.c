/* USER CODE BEGIN Header */
/**
  ******************************************************************************
  * @file           : main.c
  * @brief          : Main program body
  ******************************************************************************
  * @attention
  *
  * Copyright (c) 2022 STMicroelectronics.
  * All rights reserved.
  *
  * This software is licensed under terms that can be found in the LICENSE file
  * in the root directory of this software component.
  * If no LICENSE file comes with this software, it is provided AS-IS.
  *
  ******************************************************************************
  */
/* USER CODE END Header */
/* Includes ------------------------------------------------------------------*/
#include "main.h"
#include "usb_device.h"

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */
#include "usbd_cdc_if.h"
#include "string.h"

/* USER CODE END Includes */

/* Private typedef -----------------------------------------------------------*/
/* USER CODE BEGIN PTD */

/* USER CODE END PTD */

/* Private define ------------------------------------------------------------*/
/* USER CODE BEGIN PD */
/* USER CODE END PD */

/* Private macro -------------------------------------------------------------*/
/* USER CODE BEGIN PM */

/* USER CODE END PM */

/* Private variables ---------------------------------------------------------*/
I2C_HandleTypeDef hi2c1;

TIM_HandleTypeDef htim2;
TIM_HandleTypeDef htim3;
TIM_HandleTypeDef htim5;
TIM_HandleTypeDef htim14;

/* USER CODE BEGIN PV */

/* USER CODE END PV */

/* Private function prototypes -----------------------------------------------*/
void SystemClock_Config(void);
static void MX_GPIO_Init(void);
static void MX_I2C1_Init(void);
static void MX_TIM2_Init(void);
static void MX_TIM5_Init(void);
static void MX_TIM3_Init(void);
static void MX_TIM14_Init(void);
/* USER CODE BEGIN PFP */

/* USER CODE END PFP */

/* Private user code ---------------------------------------------------------*/
/* USER CODE BEGIN 0 */

char *data = "yolo";
uint8_t buffer[64];

uint8_t duty_edit_mode_enabled = 0;
uint8_t duty_edit_mode_enabled_buffer = 1;


extern uint32_t leds_running;

/* USER CODE END 0 */

/**
  * @brief  The application entry point.
  * @retval int
  */
int main(void)
{
  /* USER CODE BEGIN 1 */

  /* USER CODE END 1 */

  /* MCU Configuration--------------------------------------------------------*/

  /* Reset of all peripherals, Initializes the Flash interface and the Systick. */
  HAL_Init();

  /* USER CODE BEGIN Init */

  /* USER CODE END Init */

  /* Configure the system clock */
  SystemClock_Config();

  /* USER CODE BEGIN SysInit */

  /* USER CODE END SysInit */

  /* Initialize all configured peripherals */
  MX_GPIO_Init();
  MX_I2C1_Init();
  MX_TIM2_Init();
  MX_USB_DEVICE_Init();
  MX_TIM5_Init();
  MX_TIM3_Init();
  MX_TIM14_Init();
  /* USER CODE BEGIN 2 */


  if (HAL_GPIO_ReadPin(oled_addr_sel_GPIO_Port, oled_addr_sel_Pin)) {
	  // Initialize OLED
	  ssd1306_Init(0x78);
  } else {
	  // Initialize OLED
	  ssd1306_Init(0x7A);
  }


  // Initialize leds
  Leds_Init(LEDS_5V_SUPPLY, LED_MODE_SINGLE, LED_DIR_RIGHT);

  /* USER CODE END 2 */

  /* Infinite loop */
  /* USER CODE BEGIN WHILE */
  while (1)
  {
    /* USER CODE END WHILE */

    /* USER CODE BEGIN 3 */
	  update_gui();
	  HAL_Delay(10);

//	  HAL_GPIO_TogglePin(debug_led_GPIO_Port, debug_led_Pin);
//	  HAL_Delay(1000);

  }
  /* USER CODE END 3 */
}

/**
  * @brief System Clock Configuration
  * @retval None
  */
void SystemClock_Config(void)
{
  RCC_OscInitTypeDef RCC_OscInitStruct = {0};
  RCC_ClkInitTypeDef RCC_ClkInitStruct = {0};

  /** Configure the main internal regulator output voltage
  */
  __HAL_RCC_PWR_CLK_ENABLE();
  __HAL_PWR_VOLTAGESCALING_CONFIG(PWR_REGULATOR_VOLTAGE_SCALE1);

  /** Initializes the RCC Oscillators according to the specified parameters
  * in the RCC_OscInitTypeDef structure.
  */
  RCC_OscInitStruct.OscillatorType = RCC_OSCILLATORTYPE_HSE;
  RCC_OscInitStruct.HSEState = RCC_HSE_ON;
  RCC_OscInitStruct.PLL.PLLState = RCC_PLL_ON;
  RCC_OscInitStruct.PLL.PLLSource = RCC_PLLSOURCE_HSE;
  RCC_OscInitStruct.PLL.PLLM = 4;
  RCC_OscInitStruct.PLL.PLLN = 72;
  RCC_OscInitStruct.PLL.PLLP = RCC_PLLP_DIV2;
  RCC_OscInitStruct.PLL.PLLQ = 3;
  if (HAL_RCC_OscConfig(&RCC_OscInitStruct) != HAL_OK)
  {
    Error_Handler();
  }

  /** Initializes the CPU, AHB and APB buses clocks
  */
  RCC_ClkInitStruct.ClockType = RCC_CLOCKTYPE_HCLK|RCC_CLOCKTYPE_SYSCLK
                              |RCC_CLOCKTYPE_PCLK1|RCC_CLOCKTYPE_PCLK2;
  RCC_ClkInitStruct.SYSCLKSource = RCC_SYSCLKSOURCE_PLLCLK;
  RCC_ClkInitStruct.AHBCLKDivider = RCC_SYSCLK_DIV1;
  RCC_ClkInitStruct.APB1CLKDivider = RCC_HCLK_DIV2;
  RCC_ClkInitStruct.APB2CLKDivider = RCC_HCLK_DIV1;

  if (HAL_RCC_ClockConfig(&RCC_ClkInitStruct, FLASH_LATENCY_2) != HAL_OK)
  {
    Error_Handler();
  }
}

/**
  * @brief I2C1 Initialization Function
  * @param None
  * @retval None
  */
static void MX_I2C1_Init(void)
{

  /* USER CODE BEGIN I2C1_Init 0 */

  /* USER CODE END I2C1_Init 0 */

  /* USER CODE BEGIN I2C1_Init 1 */

  /* USER CODE END I2C1_Init 1 */
  hi2c1.Instance = I2C1;
  hi2c1.Init.ClockSpeed = 400000;
  hi2c1.Init.DutyCycle = I2C_DUTYCYCLE_2;
  hi2c1.Init.OwnAddress1 = 0;
  hi2c1.Init.AddressingMode = I2C_ADDRESSINGMODE_7BIT;
  hi2c1.Init.DualAddressMode = I2C_DUALADDRESS_DISABLE;
  hi2c1.Init.OwnAddress2 = 0;
  hi2c1.Init.GeneralCallMode = I2C_GENERALCALL_DISABLE;
  hi2c1.Init.NoStretchMode = I2C_NOSTRETCH_DISABLE;
  if (HAL_I2C_Init(&hi2c1) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN I2C1_Init 2 */

  /* USER CODE END I2C1_Init 2 */

}

/**
  * @brief TIM2 Initialization Function
  * @param None
  * @retval None
  */
static void MX_TIM2_Init(void)
{

  /* USER CODE BEGIN TIM2_Init 0 */

  /* USER CODE END TIM2_Init 0 */

  TIM_MasterConfigTypeDef sMasterConfig = {0};
  TIM_OC_InitTypeDef sConfigOC = {0};

  /* USER CODE BEGIN TIM2_Init 1 */

  /* USER CODE END TIM2_Init 1 */
  htim2.Instance = TIM2;
  htim2.Init.Prescaler = 72-1;
  htim2.Init.CounterMode = TIM_COUNTERMODE_UP;
  htim2.Init.Period = 100000-1;
  htim2.Init.ClockDivision = TIM_CLOCKDIVISION_DIV1;
  htim2.Init.AutoReloadPreload = TIM_AUTORELOAD_PRELOAD_ENABLE;
  if (HAL_TIM_OC_Init(&htim2) != HAL_OK)
  {
    Error_Handler();
  }
  if (HAL_TIM_PWM_Init(&htim2) != HAL_OK)
  {
    Error_Handler();
  }
  sMasterConfig.MasterOutputTrigger = TIM_TRGO_RESET;
  sMasterConfig.MasterSlaveMode = TIM_MASTERSLAVEMODE_DISABLE;
  if (HAL_TIMEx_MasterConfigSynchronization(&htim2, &sMasterConfig) != HAL_OK)
  {
    Error_Handler();
  }
  sConfigOC.OCMode = TIM_OCMODE_TIMING;
  sConfigOC.Pulse = 0;
  sConfigOC.OCPolarity = TIM_OCPOLARITY_HIGH;
  sConfigOC.OCFastMode = TIM_OCFAST_DISABLE;
  if (HAL_TIM_OC_ConfigChannel(&htim2, &sConfigOC, TIM_CHANNEL_1) != HAL_OK)
  {
    Error_Handler();
  }
  sConfigOC.OCMode = TIM_OCMODE_PWM1;
  sConfigOC.Pulse = 50000;
  if (HAL_TIM_PWM_ConfigChannel(&htim2, &sConfigOC, TIM_CHANNEL_4) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN TIM2_Init 2 */

  /* USER CODE END TIM2_Init 2 */
  HAL_TIM_MspPostInit(&htim2);

}

/**
  * @brief TIM3 Initialization Function
  * @param None
  * @retval None
  */
static void MX_TIM3_Init(void)
{

  /* USER CODE BEGIN TIM3_Init 0 */

  /* USER CODE END TIM3_Init 0 */

  TIM_ClockConfigTypeDef sClockSourceConfig = {0};
  TIM_MasterConfigTypeDef sMasterConfig = {0};
  TIM_OC_InitTypeDef sConfigOC = {0};

  /* USER CODE BEGIN TIM3_Init 1 */

  /* USER CODE END TIM3_Init 1 */
  htim3.Instance = TIM3;
  htim3.Init.Prescaler = 7200-1;
  htim3.Init.CounterMode = TIM_COUNTERMODE_UP;
  htim3.Init.Period = 15000-1;
  htim3.Init.ClockDivision = TIM_CLOCKDIVISION_DIV1;
  htim3.Init.AutoReloadPreload = TIM_AUTORELOAD_PRELOAD_DISABLE;
  if (HAL_TIM_Base_Init(&htim3) != HAL_OK)
  {
    Error_Handler();
  }
  sClockSourceConfig.ClockSource = TIM_CLOCKSOURCE_INTERNAL;
  if (HAL_TIM_ConfigClockSource(&htim3, &sClockSourceConfig) != HAL_OK)
  {
    Error_Handler();
  }
  if (HAL_TIM_OC_Init(&htim3) != HAL_OK)
  {
    Error_Handler();
  }
  sMasterConfig.MasterOutputTrigger = TIM_TRGO_RESET;
  sMasterConfig.MasterSlaveMode = TIM_MASTERSLAVEMODE_DISABLE;
  if (HAL_TIMEx_MasterConfigSynchronization(&htim3, &sMasterConfig) != HAL_OK)
  {
    Error_Handler();
  }
  sConfigOC.OCMode = TIM_OCMODE_TIMING;
  sConfigOC.Pulse = 0;
  sConfigOC.OCPolarity = TIM_OCPOLARITY_HIGH;
  sConfigOC.OCFastMode = TIM_OCFAST_DISABLE;
  if (HAL_TIM_OC_ConfigChannel(&htim3, &sConfigOC, TIM_CHANNEL_1) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN TIM3_Init 2 */

  __HAL_TIM_CLEAR_FLAG(&htim3, TIM_SR_UIF); // Fixes immediate interrupt on starting this timer

  /* USER CODE END TIM3_Init 2 */

}

/**
  * @brief TIM5 Initialization Function
  * @param None
  * @retval None
  */
static void MX_TIM5_Init(void)
{

  /* USER CODE BEGIN TIM5_Init 0 */

  /* USER CODE END TIM5_Init 0 */

  TIM_ClockConfigTypeDef sClockSourceConfig = {0};
  TIM_MasterConfigTypeDef sMasterConfig = {0};
  TIM_OC_InitTypeDef sConfigOC = {0};

  /* USER CODE BEGIN TIM5_Init 1 */

  /* USER CODE END TIM5_Init 1 */
  htim5.Instance = TIM5;
  htim5.Init.Prescaler = 72-1;
  htim5.Init.CounterMode = TIM_COUNTERMODE_UP;
  htim5.Init.Period = 1000000-1;
  htim5.Init.ClockDivision = TIM_CLOCKDIVISION_DIV1;
  htim5.Init.AutoReloadPreload = TIM_AUTORELOAD_PRELOAD_DISABLE;
  if (HAL_TIM_Base_Init(&htim5) != HAL_OK)
  {
    Error_Handler();
  }
  sClockSourceConfig.ClockSource = TIM_CLOCKSOURCE_INTERNAL;
  if (HAL_TIM_ConfigClockSource(&htim5, &sClockSourceConfig) != HAL_OK)
  {
    Error_Handler();
  }
  if (HAL_TIM_OC_Init(&htim5) != HAL_OK)
  {
    Error_Handler();
  }
  sMasterConfig.MasterOutputTrigger = TIM_TRGO_RESET;
  sMasterConfig.MasterSlaveMode = TIM_MASTERSLAVEMODE_DISABLE;
  if (HAL_TIMEx_MasterConfigSynchronization(&htim5, &sMasterConfig) != HAL_OK)
  {
    Error_Handler();
  }
  sConfigOC.OCMode = TIM_OCMODE_TIMING;
  sConfigOC.Pulse = 0;
  sConfigOC.OCPolarity = TIM_OCPOLARITY_HIGH;
  sConfigOC.OCFastMode = TIM_OCFAST_DISABLE;
  if (HAL_TIM_OC_ConfigChannel(&htim5, &sConfigOC, TIM_CHANNEL_1) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN TIM5_Init 2 */

  __HAL_TIM_CLEAR_FLAG(&htim5, TIM_SR_UIF); // Fixes immediate interrupt on starting this timer

  /* USER CODE END TIM5_Init 2 */

}

/**
  * @brief TIM14 Initialization Function
  * @param None
  * @retval None
  */
static void MX_TIM14_Init(void)
{

  /* USER CODE BEGIN TIM14_Init 0 */

  /* USER CODE END TIM14_Init 0 */

  TIM_OC_InitTypeDef sConfigOC = {0};

  /* USER CODE BEGIN TIM14_Init 1 */

  /* USER CODE END TIM14_Init 1 */
  htim14.Instance = TIM14;
  htim14.Init.Prescaler = 72-1;
  htim14.Init.CounterMode = TIM_COUNTERMODE_UP;
  htim14.Init.Period = 30-1;
  htim14.Init.ClockDivision = TIM_CLOCKDIVISION_DIV1;
  htim14.Init.AutoReloadPreload = TIM_AUTORELOAD_PRELOAD_DISABLE;
  if (HAL_TIM_Base_Init(&htim14) != HAL_OK)
  {
    Error_Handler();
  }
  if (HAL_TIM_OC_Init(&htim14) != HAL_OK)
  {
    Error_Handler();
  }
  sConfigOC.OCMode = TIM_OCMODE_TIMING;
  sConfigOC.Pulse = 0;
  sConfigOC.OCPolarity = TIM_OCPOLARITY_HIGH;
  sConfigOC.OCFastMode = TIM_OCFAST_DISABLE;
  if (HAL_TIM_OC_ConfigChannel(&htim14, &sConfigOC, TIM_CHANNEL_1) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN TIM14_Init 2 */

  /* USER CODE END TIM14_Init 2 */

}

/**
  * @brief GPIO Initialization Function
  * @param None
  * @retval None
  */
static void MX_GPIO_Init(void)
{
  GPIO_InitTypeDef GPIO_InitStruct = {0};
/* USER CODE BEGIN MX_GPIO_Init_1 */
/* USER CODE END MX_GPIO_Init_1 */

  /* GPIO Ports Clock Enable */
  __HAL_RCC_GPIOH_CLK_ENABLE();
  __HAL_RCC_GPIOA_CLK_ENABLE();
  __HAL_RCC_GPIOC_CLK_ENABLE();
  __HAL_RCC_GPIOB_CLK_ENABLE();

  /*Configure GPIO pin Output Level */
  HAL_GPIO_WritePin(GPIOA, en_row_1_Pin|en_row_2_Pin|en_row_3_Pin|en_row_4_Pin
                          |en_row_5_Pin|en_row_6_Pin|sr_data_Pin|sr_latch_Pin, GPIO_PIN_RESET);

  /*Configure GPIO pin Output Level */
  HAL_GPIO_WritePin(GPIOC, sr_clock_Pin|sr_enable_Pin|dbg_out_Pin|dbg_out2_Pin
                          |ttl_out_Pin, GPIO_PIN_RESET);

  /*Configure GPIO pin Output Level */
  HAL_GPIO_WritePin(enc_c_GPIO_Port, enc_c_Pin, GPIO_PIN_RESET);

  /*Configure GPIO pin Output Level */
  HAL_GPIO_WritePin(debug_led_GPIO_Port, debug_led_Pin, GPIO_PIN_SET);

  /*Configure GPIO pins : en_row_1_Pin en_row_2_Pin en_row_3_Pin en_row_4_Pin
                           en_row_5_Pin en_row_6_Pin */
  GPIO_InitStruct.Pin = en_row_1_Pin|en_row_2_Pin|en_row_3_Pin|en_row_4_Pin
                          |en_row_5_Pin|en_row_6_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_VERY_HIGH;
  HAL_GPIO_Init(GPIOA, &GPIO_InitStruct);

  /*Configure GPIO pins : sr_data_Pin sr_latch_Pin */
  GPIO_InitStruct.Pin = sr_data_Pin|sr_latch_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
  HAL_GPIO_Init(GPIOA, &GPIO_InitStruct);

  /*Configure GPIO pins : sr_clock_Pin sr_enable_Pin dbg_out_Pin dbg_out2_Pin */
  GPIO_InitStruct.Pin = sr_clock_Pin|sr_enable_Pin|dbg_out_Pin|dbg_out2_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
  HAL_GPIO_Init(GPIOC, &GPIO_InitStruct);

  /*Configure GPIO pin : sw_enc_Pin */
  GPIO_InitStruct.Pin = sw_enc_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_IT_FALLING;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  HAL_GPIO_Init(sw_enc_GPIO_Port, &GPIO_InitStruct);

  /*Configure GPIO pins : enc_b_Pin enc_a_Pin */
  GPIO_InitStruct.Pin = enc_b_Pin|enc_a_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_IT_FALLING;
  GPIO_InitStruct.Pull = GPIO_PULLUP;
  HAL_GPIO_Init(GPIOB, &GPIO_InitStruct);

  /*Configure GPIO pin : enc_c_Pin */
  GPIO_InitStruct.Pin = enc_c_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Pull = GPIO_PULLDOWN;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
  HAL_GPIO_Init(enc_c_GPIO_Port, &GPIO_InitStruct);

  /*Configure GPIO pin : debug_led_Pin */
  GPIO_InitStruct.Pin = debug_led_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
  HAL_GPIO_Init(debug_led_GPIO_Port, &GPIO_InitStruct);

  /*Configure GPIO pin : oled_addr_sel_Pin */
  GPIO_InitStruct.Pin = oled_addr_sel_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_INPUT;
  GPIO_InitStruct.Pull = GPIO_PULLUP;
  HAL_GPIO_Init(oled_addr_sel_GPIO_Port, &GPIO_InitStruct);

  /*Configure GPIO pin : ttl_out_Pin */
  GPIO_InitStruct.Pin = ttl_out_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_HIGH;
  HAL_GPIO_Init(ttl_out_GPIO_Port, &GPIO_InitStruct);

  /*Configure GPIO pin : ttl_in_Pin */
  GPIO_InitStruct.Pin = ttl_in_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_IT_RISING_FALLING;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  HAL_GPIO_Init(ttl_in_GPIO_Port, &GPIO_InitStruct);

  /*Configure GPIO pins : sw_r_Pin sw_l_Pin */
  GPIO_InitStruct.Pin = sw_r_Pin|sw_l_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_IT_RISING_FALLING;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  HAL_GPIO_Init(GPIOB, &GPIO_InitStruct);

  /* EXTI interrupt init*/
  HAL_NVIC_SetPriority(EXTI0_IRQn, 0, 0);
  HAL_NVIC_EnableIRQ(EXTI0_IRQn);

  HAL_NVIC_SetPriority(EXTI1_IRQn, 0, 0);
  HAL_NVIC_EnableIRQ(EXTI1_IRQn);

  HAL_NVIC_SetPriority(EXTI9_5_IRQn, 0, 0);
  HAL_NVIC_EnableIRQ(EXTI9_5_IRQn);

  HAL_NVIC_SetPriority(EXTI15_10_IRQn, 0, 0);
  HAL_NVIC_EnableIRQ(EXTI15_10_IRQn);

/* USER CODE BEGIN MX_GPIO_Init_2 */
/* USER CODE END MX_GPIO_Init_2 */
}

/* USER CODE BEGIN 4 */
void HAL_GPIO_EXTI_Callback(uint16_t GPIO_Pin)
{


	if (GPIO_Pin == ttl_in_Pin) {
//		if (!HAL_GPIO_ReadPin(ttl_in_GPIO_Port, ttl_in_Pin) == trigger_edge) {
////			HAL_GPIO_TogglePin(debug_led_GPIO_Port, debug_led_Pin);
//			HAL_GPIO_WritePin(debug_led_GPIO_Port, debug_led_Pin, GPIO_PIN_SET);
////			Leds_Stop();
//			Leds_Init_Direction(led_direction);
//			Leds_Start();
//		} else {
////			HAL_GPIO_TogglePin(debug_led_GPIO_Port, debug_led_Pin);
//			HAL_GPIO_WritePin(debug_led_GPIO_Port, debug_led_Pin, GPIO_PIN_RESET);
//		}

//		uint32_t detected_edge = !HAL_GPIO_ReadPin(ttl_in_GPIO_Port, ttl_in_Pin);
		uint32_t detected_edge = (ttl_in_GPIO_Port->IDR & ttl_in_Pin);
//		HAL_GPIO_WritePin(debug_led_GPIO_Port, debug_led_Pin, detected_edge);

		if (detected_edge != input_trigger_edge) {
//			if (trigger_edge == TRIGGER_EDGE_RISING) {
				Leds_Stop();
//				Leds_Init_Direction(led_direction);
//				if (led_mode == LED_MODE_SINGLE) {
//						Leds_Set_Rows(LED_ROW_1);
//						sr_data_offset = 0;
//				}
				Leds_Reset();

				Leds_Start();
//			}
		}
	return;
	}

	GPIO_PinState state = GPIO_PIN_RESET;

//	if (GPIO_Pin == sw_enc_Pin) {
//		Leds_Stop();
//		Leds_Rotate();
//	}

	if (GPIO_Pin == sw_r_Pin) {
		state = HAL_GPIO_ReadPin(sw_r_GPIO_Port, sw_r_Pin);
		if (!state) { // FALLING EDGE
			htim5.Instance->CNT = 1;
			HAL_TIM_Base_Start_IT(&htim5);
			duty_edit_mode_enabled = 0;
//			htim5.Instance->CNT = 1;
//			HAL_TIM_OnePulse_Start_IT(&htim5, TIM_CHANNEL_1);

		} else { // RISING EDGE
			if (duty_edit_mode_enabled == 0) {
				Leds_Stop();
				Leds_Toggle_Direction();
				Leds_Start();
				HAL_TIM_Base_Stop_IT(&htim5);
//				htim5.Instance->CNT = 0;
			} else {
				duty_edit_mode_enabled = 0;
			}
		}
	}

	if (GPIO_Pin == sw_l_Pin) {
		state = HAL_GPIO_ReadPin(sw_l_GPIO_Port, sw_l_Pin);
		if (!state) { // FALLING EDGE

		} else { // RISING EDGE
			Leds_Stop();
			Leds_Toggle_Mode();
			Leds_Start();
		}
	}

	// if encoder pin a triggered interrupt
	if (GPIO_Pin == enc_a_Pin) {
		if (!HAL_GPIO_ReadPin(enc_b_GPIO_Port, enc_b_Pin)) {
			if (duty_edit_mode_enabled == 0) { // increase frame length
				uint32_t multiplyer = 1;

				if (step_us >= STEP_SLOW_SPEED_THRESHOLD) {
					multiplyer = 10;
				}

				int coarse = !HAL_GPIO_ReadPin(sw_enc_GPIO_Port, sw_enc_Pin);
				if (coarse) {
					step_us = step_us - (ENCODER_COARSE_STEP * multiplyer);
				} else {
					step_us = step_us - (1 * multiplyer);
				}

				// wrap values around if lower limit passed
				if (step_us < STEP_MIN_US) {
					step_us = STEP_MAX_US - (STEP_MIN_US - step_us) + !coarse;
				}
			} else { // Decrease pulse length
				//				HAL_GPIO_TogglePin(debug_led_GPIO_Port, debug_led_Pin);
				if (duty > PWM_DUTY_STEP) {
					duty = duty - PWM_DUTY_STEP;
				}
			}

		}
	}

	// if encoder pin b triggered interrupt
	if (GPIO_Pin == enc_b_Pin) {
		if (!HAL_GPIO_ReadPin(enc_a_GPIO_Port, enc_a_Pin)) {
			if (duty_edit_mode_enabled == 0) { // increase frame length
				uint32_t multiplyer = 1;

				if (step_us >= STEP_SLOW_SPEED_THRESHOLD) {
					multiplyer = 10;
				}

				int coarse = !HAL_GPIO_ReadPin(sw_enc_GPIO_Port, sw_enc_Pin);
				if (coarse) {
					step_us = step_us + (ENCODER_COARSE_STEP * multiplyer);
				} else {
					step_us = step_us + (1 * multiplyer);
				}

				// wrap values around if higher limit passed
				if (step_us > STEP_MAX_US) {
					step_us = step_us - STEP_MAX_US + STEP_MIN_US - !coarse;
				}
			} else { // Increase pulse length
//				HAL_GPIO_TogglePin(debug_led_GPIO_Port, debug_led_Pin);
				if (duty < 100) {
					duty = duty + PWM_DUTY_STEP;
				}
			}

		}
	}


}

void HAL_TIM_PeriodElapsedCallback (TIM_HandleTypeDef * htim) {
	if (htim == &htim2) {
		Leds_Rotate();
//		HAL_GPIO_TogglePin(dbg_out_GPIO_Port, dbg_out_Pin);
	}

	if (htim == &htim14) {
		HAL_GPIO_WritePin(ttl_out_GPIO_Port, ttl_out_Pin, (output_trigger_edge == TRIGGER_EDGE_RISING));
		HAL_TIM_Base_Stop_IT(&htim14);
	}

	if (htim == &htim5) {
		HAL_TIM_Base_Stop_IT(&htim5);
		duty_edit_mode_enabled = 1;
	}

	if (htim == &htim3) {
		HAL_TIM_Base_Stop_IT(&htim3);
		duty_edit_mode_enabled = 0;
//		draw_main_view();
	}
}

//HAL_TIM_PWM_PulseFinishedCallback (TIM_HandleTypeDef * htim) {
//	if (htim == &htim2) {
//		HAL_GPIO_WritePin(dbg_out_GPIO_Port, dbg_out_Pin, GPIO_PIN_SET);
//	}
//}
//
//HAL_TIM_PWM_PulseFinishedHalfCpltCallback (TIM_HandleTypeDef * htim) {
//	if (htim == &htim2) {
//		HAL_GPIO_WritePin(dbg_out_GPIO_Port, dbg_out_Pin, GPIO_PIN_RESET);
//	}
//}

// Update timer interval in us
void Update_Timer_Interval(uint32_t interval_us) {
	Leds_Stop();

	TIM_MasterConfigTypeDef sMasterConfig = {0};
	TIM_OC_InitTypeDef sConfigOC = {0};

	htim2.Instance = TIM2;
	htim2.Init.Prescaler = 72-1;
	htim2.Init.CounterMode = TIM_COUNTERMODE_UP;
	htim2.Init.Period = interval_us - 1;
	htim2.Init.ClockDivision = TIM_CLOCKDIVISION_DIV1;
	htim2.Init.AutoReloadPreload = TIM_AUTORELOAD_PRELOAD_DISABLE;
	if (HAL_TIM_OC_Init(&htim2) != HAL_OK)
	{
		Error_Handler();
	}
	sMasterConfig.MasterOutputTrigger = TIM_TRGO_RESET;
	sMasterConfig.MasterSlaveMode = TIM_MASTERSLAVEMODE_DISABLE;
	if (HAL_TIMEx_MasterConfigSynchronization(&htim2, &sMasterConfig) != HAL_OK)
	{
		Error_Handler();
	}
	sConfigOC.OCMode = TIM_OCMODE_TIMING;
	sConfigOC.Pulse = 0;
	sConfigOC.OCPolarity = TIM_OCPOLARITY_HIGH;
	sConfigOC.OCFastMode = TIM_OCFAST_DISABLE;
	if (HAL_TIM_OC_ConfigChannel(&htim2, &sConfigOC, TIM_CHANNEL_1) != HAL_OK)
	{
		Error_Handler();
	}

	Leds_Set_Duty(interval_us);
	Leds_Reset();
	Leds_Start();
}

void Leds_Start(void) {
	if (leds_running == LEDS_STOPPED) {

		leds_running = LEDS_RUNNING;
//		leds_running = LEDS_RUNNING;
		__HAL_TIM_SET_COUNTER(&htim2, 0);
//		__HAL_TIM_CLEAR_FLAG(&htim14, TIM_FLAG_UPDATE);
		HAL_TIM_Base_Start_IT(&htim2);
		htim2.Instance->EGR = TIM_EGR_UG; // Force an update event to trigger the interrupt immediately
	}
}

void Leds_Stop(void) {
	if (leds_running == LEDS_RUNNING) {
		leds_running = LEDS_STOPPED;
		HAL_TIM_Base_Stop_IT(&htim2);
		__HAL_TIM_SET_COUNTER(&htim2, 0);
//		HAL_TIM_PWM_Start_IT(&htim2, TIM_CHANNEL_2);
	}
}

void Led_Start_Power(void) {
	HAL_TIM_PWM_Start(&htim2, TIM_CHANNEL_4);
}

void Led_Stop_Power(void) {
	HAL_TIM_PWM_Stop(&htim2, TIM_CHANNEL_4);
}

void Set_Compare_Value(uint32_t compare_value) {
	__HAL_TIM_SET_COMPARE(&htim2, TIM_CHANNEL_4, compare_value);
}

void Start_GUI_Duty_View_Timer(void) {
	htim3.Instance->CNT = 0;
	HAL_TIM_Base_Start_IT(&htim3);
}

/* USER CODE END 4 */

/**
  * @brief  This function is executed in case of error occurrence.
  * @retval None
  */
void Error_Handler(void)
{
  /* USER CODE BEGIN Error_Handler_Debug */
  /* User can add his own implementation to report the HAL error return state */
  __disable_irq();
  while (1)
  {
	  HAL_GPIO_TogglePin(debug_led_GPIO_Port, debug_led_Pin);
	  HAL_Delay(200);
  }
  /* USER CODE END Error_Handler_Debug */
}

#ifdef  USE_FULL_ASSERT
/**
  * @brief  Reports the name of the source file and the source line number
  *         where the assert_param error has occurred.
  * @param  file: pointer to the source file name
  * @param  line: assert_param error line source number
  * @retval None
  */
void assert_failed(uint8_t *file, uint32_t line)
{
  /* USER CODE BEGIN 6 */
  /* User can add his own implementation to report the file name and line number,
     ex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */
  /* USER CODE END 6 */
}
#endif /* USE_FULL_ASSERT */
