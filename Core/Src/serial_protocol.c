/*
 * serial_protocol.c
 *
 *  Created on: Mar 24, 2022
 *      Author: Kasutaja
 */

#include "serial_protocol.h"


void parse_serial(uint8_t* Buf) {
	if (Buf[0] == CMD_START) {
		Leds_Start();
	} else if (Buf[0] == CMD_STOP) {
		Leds_Stop();
	} else if (Buf[0] == CMD_SET_DIR_LEFT) {
		Leds_Stop();
		Leds_Init_Direction(LED_DIR_LEFT);
		Leds_Start();
	} else if (Buf[0] == CMD_SET_DIR_RIGHT) {
		Leds_Stop();
		Leds_Init_Direction(LED_DIR_RIGHT);
		Leds_Start();
	} else if (Buf[0] == CMD_SET_MODE_COLUMN) {
		Leds_Set_Mode(LED_MODE_COLUMN);
	} else if (Buf[0] == CMD_SET_MODE_SINGLE) {
		Leds_Set_Mode(LED_MODE_SINGLE);
	} else if (Buf[0] == CMD_SET_INPUT_TRIGGER_EDGE) {
		Leds_Set_Input_Trigger_Edge(Buf[1] - 48);
	} else if (Buf[0] == CMD_SET_OUTPUT_TRIGGER_EDGE) {
		Leds_Set_Output_Trigger_Edge(Buf[1] - 48);
	} else if (Buf[0] == CMD_SET_FRAME_LENGTH) {
		uint8_t value_buf[64];

		int i = 0;
		for (; Buf[i + 1] != NEWLINE; i++) {
			value_buf[i] = Buf[i + 1];
		}

		value_buf[i] = NEWLINE;
		uint32_t temp_step_us = atoi(value_buf);
		if ((temp_step_us >= STEP_MIN_US) && (temp_step_us <= STEP_MAX_US)) {
			step_us = temp_step_us;
			Update_Timer_Interval(step_us);
		}

	} else if (Buf[0] == CMD_SET_BRIGHTNESS) {
		uint8_t value_buf[64];

		int i = 0;
		for (; Buf[i + 1] != NEWLINE; i++) {
			value_buf[i] = Buf[i + 1];
		}

		value_buf[i] = NEWLINE;
		duty = atoi(value_buf);
		uint32_t temp_duty = atoi(value_buf);

		if ((temp_duty <= 100) && (temp_duty >= PWM_DUTY_STEP)) {
			duty = temp_duty;
			Leds_Set_Duty(step_us);
//			draw_duty_view();
			duty_edit_mode_enabled = 1;
			Start_GUI_Duty_View_Timer();
		}

	}
}
